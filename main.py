import requests
import tkinter as tk

OMDB_API_KEY = ""  # اینجا کلید API خود را قرار دهید

def get_movie_or_series_data(title):
    url = f"http://www.omdbapi.com/?apikey={OMDB_API_KEY}&t={title}"
    response = requests.get(url)
    data = response.json()
    if data["Response"] == "False":
        return "Movie or series not found!"
    else:
        result = f"Title: {data['Title']}\n"
        result += f"Year: {data['Year']}\n"
        result += f"Rated: {data['Rated']}\n"
        result += f"Released: {data['Released']}\n"
        result += f"Runtime: {data['Runtime']}\n"
        result += f"Genre: {data['Genre']}\n"
        result += f"Director: {data['Director']}\n"
        result += f"Writer: {data['Writer']}\n"
        result += f"Actors: {data['Actors']}\n"
        result += f"Plot: {data['Plot']}\n"
        result += f"Language: {data['Language']}\n"
        result += f"Country: {data['Country']}\n"
        result += f"Awards: {data['Awards']}\n"
        result += f"IMDB Rating: {data['imdbRating']}\n"
        result += f"IMDB Votes: {data['imdbVotes']}\n"
        return result

def show_movie_or_series_info():
    title = entry.get()
    info = get_movie_or_series_data(title)
    status_bar.config(text=info)
    file=open("elite.txt","w")    
    file.write(info) 

root = tk.Tk()
root.title("Movie and Series Info")
root.geometry("400x200")

label = tk.Label(root, text="Enter movie or series title:")
label.pack()

entry = tk.Entry(root, width=50)
entry.pack()

button = tk.Button(root, text="Show Info", command=show_movie_or_series_info)
button.pack()

status_bar = tk.Label(root, text="", bd=1, relief=tk.SUNKEN, anchor=tk.W)
status_bar.pack(side=tk.BOTTOM, fill=tk.X)

root.mainloop()
